package com.magtech.andrew.flute;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.util.ArrayList;

/**
 * Created by andrew on 09.07.18.
 */

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>{

    ArrayList<LightUser> data = new ArrayList<>();
    View.OnClickListener listener;
    View lastSeen;

    public NewsAdapter(ArrayList<LightUser> arr) {
        if (arr != null) {
            data.addAll(arr);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<LightUser> list){
        if (list != null) {
            data = list;
        }
        notifyDataSetChanged();
    }

    public void insertItem(int pos, LightUser u){
        data.add(pos, u);
        notifyItemInserted(pos);
    }

    public void changeItem(int pos, LightUser u){
        data.set(pos, u);
        notifyItemChanged(pos);
    }

    @Override
    public NewsAdapter.NewsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.news_item, parent, false);
        return new NewsAdapter.NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(NewsAdapter.NewsViewHolder holder, int position) {
        LightUser u = data.get(position);
        if (position == data.size()-1) {
            lastSeen = holder.itemView;
        }
        holder.fName.setText(u.first_name);
        holder.sName.setText(u.last_name);
        holder.userAva.setImageBitmap(u.smallPhoto);
        holder.nStories.setText(Integer.toString(u.numStories));
        holder.itemView.setTag(u.ID);
    }

    public void setOnItemClickListener(View.OnClickListener ocl){
        listener = ocl;
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder{
        TextView fName, sName, nStories;
        ImageView userAva;
        public NewsViewHolder(View itemView) {
            super(itemView);
            fName = itemView.findViewById(R.id.savesFName);
            sName = itemView.findViewById(R.id.savesSName);
            userAva = itemView.findViewById(R.id.userAva);
            nStories = itemView.findViewById(R.id.text_num_stories);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view);
                }
            });
        }
    }
}
