package com.magtech.andrew.flute;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;

import com.magtech.andrew.flute.MT_VK_API.userLists.Friends;
import com.magtech.andrew.flute.MT_VK_API.userLists.UserLoaderable;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.util.ArrayList;

public class StoriesSettingsActivity extends AppCompatActivity {

    ArrayList<Integer> userIDs = new ArrayList<>();
    UserLoaderable userLoaderable = new Friends();
    TextView textLoaded, textAll;
    SwipeRefreshLayout srl;
    RecyclerView savesList;
    StoriesSettingsAdapter ssa;
    LinearLayoutManager llm;
    NestedScrollView nsv;
    EditText edit;
    Button butMore, butConf;
    ArrayList<LightUser> usersList = new ArrayList<>();
    boolean isSelectedAll = false;
    int offset = 0;
    static int size;
    Port port = new Port();
    long time1, time2;

    View.OnClickListener listener1 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Switch sw = (Switch) view.getTag();
            if (sw.isChecked()) {
                sw.setChecked(false);
                System.out.println("CLICKED TAG ON: "+Integer.toString((Integer) sw.getTag()));
                for (int i=0; i<userIDs.size(); i++){
                    if (userIDs.get(i) == ((Integer) sw.getTag())) {
                        userIDs.remove(i);
                        System.out.println("REMOVED: "+userIDs);
                    }
                }
            }
            else {
                sw.setChecked(true);
                System.out.println("CLICKED TAG OFF: "+Integer.toString((Integer) sw.getTag()));
                boolean isContains = false;
                for (int i=0; i<userIDs.size(); i++){
                    if (userIDs.get(i) == ((Integer) sw.getTag())) {
                        isContains = true;
                    }
                }
                if (!isContains) {
                    userIDs.add((Integer) sw.getTag());
                    System.out.println("ADDED: "+userIDs);
                }
            }
        }
    };

    View.OnClickListener listener2 = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Switch sw = (Switch) view;
            if (sw.isChecked()) {
                System.out.println("CLICKED TAG OFF: "+Integer.toString((Integer) sw.getTag()));
                boolean isContains = false;
                for (int i=0; i<userIDs.size(); i++){
                    if (userIDs.get(i) == ((Integer) sw.getTag())) {
                        isContains = true;
                    }
                }
                if (!isContains) {
                    userIDs.add((Integer) sw.getTag());
                    System.out.println("ADDED: "+userIDs);
                }
            }
            else {
                System.out.println("CLICKED TAG ON: "+Integer.toString((Integer) sw.getTag()));
                for (int i=0; i<userIDs.size(); i++){
                    if (userIDs.get(i) == ((Integer) sw.getTag())) {
                        userIDs.remove(i);
                        System.out.println("REMOVED: "+userIDs);
                    }
                }
            }
        }
    };

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_stories_settings);

        ActionBar ab = getSupportActionBar();
        ab.setDisplayHomeAsUpEnabled(false);
        ab.setCustomView(R.layout.saves_app_bar_layout);
        ab.setDisplayShowCustomEnabled(true);
        butConf = findViewById(R.id.butConf);

        srl = findViewById(R.id.swipe_saves);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                offset = 0;
                avaPos = 0;
                adapterPos = 0;
                usersList.clear();
                ssa = null;
                System.gc();
                FriendsDownloader fd = new FriendsDownloader();
                fd.execute(Integer.toString(Constants.autID));
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_orange_light, android.R.color.holo_red_light);

        textLoaded =  findViewById(R.id.textLoaded);
        textAll =  findViewById(R.id.textAll);
        FloatingActionButton fabToStart =  findViewById(R.id.fabToStart);
        fabToStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nsv.scrollTo(0, 0);
            }
        });
        savesList =  findViewById(R.id.savesList);
        llm = new LinearLayoutManager(this);
        nsv =  findViewById(R.id.mNestedView);
        savesList.setLayoutManager(llm);
        savesList.setItemAnimator(null);

        edit =  findViewById(R.id.tfFind);

        FriendsDownloader fd = new FriendsDownloader();
        fd.execute(Integer.toString(Constants.autID));

        edit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {}

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                String text = charSequence.toString();
                ArrayList<LightUser> data = new ArrayList<>();
                if (text.isEmpty()){
                    savesList.setAdapter(ssa);
                    System.gc();
                    textLoaded.setText(Integer.toString(ssa.data.size()));
                    if (ssa.data.size() == usersList.size())
                        butMore.setText("Конец списка");
                    else {
                        butMore.setText("Загрузить еще");
                        butMore.setEnabled(true);
                    }
                }
                else{
                    for (LightUser u : usersList) {
                        if (Integer.toString(u.ID).indexOf(text) == 0)
                            data.add(u);
                        else if (u.first_name.toLowerCase().indexOf(text.toLowerCase()) == 0)
                            data.add(u);
                        else if (u.last_name.toLowerCase().indexOf(text.toLowerCase()) == 0)
                            data.add(u);
                    }
                    StoriesSettingsAdapter ffa = new StoriesSettingsAdapter(data, true);
                    ffa.setOnItemClickListener(listener1);
                    ffa.setOnSwitchClickListener(listener2);
                    savesList.setAdapter(ffa);
                    butMore.setText("Поиск");
                    butMore.setEnabled(false);
                    textLoaded.setText(Integer.toString(data.size()));
                }
            }

            @Override
            public void afterTextChanged(Editable editable) {}
        });
        Button butDelete =  findViewById(R.id.butFindDelete);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                edit.setText("");
            }
        });

        butMore =  findViewById(R.id.buttonMore);
        butMore.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (adapterPos != size-1) {
                    time1 = System.currentTimeMillis();
                    srl.setRefreshing(true);
                    edit.setEnabled(false);
                    offset += Constants.deltaLists;
                    if (adapterPos == offset - 1) {
                        adapterPos++;
                        adapterHandler.post(new adapterThread());
                    }
                    if (avaPos == offset - 1) {
                        avaPos++;
                        new Thread(new avaLoaderThread()).start();
                    }
                }
            }
        });
    }

    public void onSelectAllClick(View v){
        Button b = (Button) v;
        userIDs.clear();
        if (b.getText().toString().equals("Выделить всех")){
            for (LightUser u:usersList)
                userIDs.add(u.ID);
            for (int i=0; i<ssa.getItemCount(); i++){
                ((Switch) llm.findViewByPosition(i).getTag()).setChecked(true);
            }
            b.setText("Отменить всех");
            isSelectedAll = true;
        }
        else{
            for (int i=0; i<ssa.getItemCount(); i++){
                ((Switch) llm.findViewByPosition(i).getTag()).setChecked(false);
            }
            b.setText("Выделить всех");
            isSelectedAll = false;
        }
    }

    public void onConfClick(View v){
        Intent data = new Intent();
        int[] user_ids = new int[userIDs.size()];
        for (int i=0; i<userIDs.size(); i++){
            user_ids[i] = userIDs.get(i);
        }
        System.out.print("USERS: ");
        for (int i=0; i<user_ids.length; i++){
            System.out.print(user_ids[i]+" ");
        }
        System.out.println();
        data.putExtra(this.getString(R.string.app_pref) + ".Users", user_ids);
        setResult(RESULT_OK, data);
        finish();
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        Intent data = new Intent();
        int[] user_ids = new int[userIDs.size()];
        for (int i=0; i<userIDs.size(); i++){
            user_ids[i] = userIDs.get(i);
        }
        System.out.print("USERS: ");
        for (int i=0; i<user_ids.length; i++){
            System.out.print(user_ids[i]+" ");
        }
        System.out.println();
        data.putExtra(this.getString(R.string.app_pref) + ".Users", user_ids);
        setResult(RESULT_OK, data);
        finish();
    }

    class FriendsDownloader extends AsyncTask<String, Integer, Boolean> {

        @Override
        public void onPreExecute(){
            super.onPreExecute();
            time1 = System.currentTimeMillis();
            srl.setRefreshing(true);
            edit.setEnabled(false);
        }

        @Override
        public Boolean doInBackground(String...param){
            usersList = userLoaderable.getUsersList(param[0]);
            size = usersList.size();
            return true;
        }

        @Override
        public void onPostExecute(Boolean b){
            super.onPostExecute(b);
            ssa = new StoriesSettingsAdapter(new ArrayList<LightUser>(), false);
            ssa.setOnItemClickListener(listener1);
            ssa.setOnSwitchClickListener(listener2);
            savesList.setAdapter(ssa);
            textAll.setText(Integer.toString(size));
            if (size != 0) {
                adapterHandler.post(new adapterThread());
                new Thread(new avaLoaderThread()).start();
            }
            else {
                srl.setRefreshing(false);
                butMore.setText("Друзей нет");
            }
        }
    }

    Handler adapterHandler = new Handler();
    int adapterPos = 0;

    class adapterThread implements Runnable{

        @Override
        public void run() {
            LightUser u = usersList.get(adapterPos);
            port.add(u);
            ssa.insertItem(adapterPos, u, isSelectedAll);
            if (adapterPos < size-1 && adapterPos < offset + Constants.deltaLists - 1){
                adapterPos++;
                adapterHandler.post(this);
            }
            else{
                textLoaded.setText(Integer.toString(adapterPos+1));
                butMore.setText("Загрузить еще");
                if (adapterPos == size-1){
                    butMore.setText("Конец списка");
                }
            }
        }
    }

    Handler avaLoaderHandler = new Handler();
    int avaPos = 0;

    class avaLoaderThread implements Runnable{

        @Override
        public void run() {
            while (port.isEmpty()) {}
            final LightUser u = port.get();
            final int a = avaPos;
            u.smallPhoto = MainActivity.downloadImage(u.avaSmall);
            avaLoaderHandler.post(new Runnable() {
                @Override
                public void run() {
                    ssa.changeItem(a, u);
                }
            });
            if (avaPos < size-1 && avaPos < offset + Constants.deltaLists - 1){
                avaPos++;
                new Thread(new avaLoaderThread()).start();
            }
            else {
                avaLoaderHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                        edit.setEnabled(true);
                        time2 = System.currentTimeMillis() - time1;
                        System.out.println("TIME: " + time2);
                    }
                });
            }
        }
    }
}
