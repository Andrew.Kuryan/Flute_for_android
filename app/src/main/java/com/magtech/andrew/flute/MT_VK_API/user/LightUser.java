package com.magtech.andrew.flute.MT_VK_API.user;

import android.graphics.Bitmap;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.requestErrors;

import java.io.InputStream;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 24.06.18.
 */

public class LightUser{
    public int ID;

    public String  first_name = "",
            last_name = "",
            avaSmall = "",
            domain = "";
    public int online, numStories;
    public long last_seen;
    public Bitmap smallPhoto = null;

    public LightUser(){}

    public LightUser(String screen_name){
        try {
            JsonObject object;
            String url = "https://api.vk.com/method/users.get?user_ids=" + screen_name +
                    "&fields=photo_100,online,last_seen,domain"
                    + Constants.token + "&v=5.8";
            URL address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            getLightInfo(object.getJsonArray("response").getJsonObject(0));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    void getLightInfo(JsonObject object){
        ID = object.getInt("id");
        first_name = object.getString("first_name");
        last_name = object.getString("last_name");
        if (object.containsKey("photo_100")) avaSmall = object.getString("photo_100");
        if (object.containsKey("domain")) domain = object.getString("domain");
        if (object.containsKey("online")) online = object.getInt("online");
        if (object.containsKey("last_seen")) last_seen = object.getJsonObject("last_seen").getInt("time");
    }

    public LightUser(JsonObject object){
        try {
            ID = object.getInt("id");
            first_name = object.getString("first_name");
            last_name = object.getString("last_name");
            if (object.containsKey("photo_100")) avaSmall = object.getString("photo_100");
            if (object.containsKey("domain")) domain = object.getString("domain");
            if (!object.containsKey("deactivated")) {
                if (object.containsKey("online")) online = object.getInt("online");
                if (object.containsKey("last_seen"))
                    last_seen = object.getJsonObject("last_seen").getInt("time");
            }
        }catch (Exception exc){
            exc.printStackTrace();
        }
    }

    public String toString() {
        return first_name + " " + last_name;
    }
}
