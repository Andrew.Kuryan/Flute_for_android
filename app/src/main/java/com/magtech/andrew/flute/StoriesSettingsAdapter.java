package com.magtech.andrew.flute;

import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;

import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.util.ArrayList;

/**
 * Created by andrew on 26.06.18.
 */

public class StoriesSettingsAdapter extends RecyclerView.Adapter<StoriesSettingsAdapter.StoriesSettingsViewHolder>{

    boolean isDownloadNeeded = false, isSelectedAll;
    ArrayList<LightUser> data = new ArrayList<>();
    View.OnClickListener listener, swListener;
    View lastSeen;

    public StoriesSettingsAdapter(ArrayList<LightUser> arr, boolean b) {
        isDownloadNeeded = b;
        if (arr != null && b) {
            data.addAll(arr);
        }
        else if (arr != null){
            data.addAll(arr);
        }
    }

    @Override
    public int getItemCount() {
        return data.size();
    }

    public void setData(ArrayList<LightUser> list){
        if (list != null) {
            data = list;
        }
        notifyDataSetChanged();
    }

    public void insertItem(int pos, LightUser u, boolean isSelected){
        data.add(pos, u);
        isSelectedAll = isSelected;
        notifyItemInserted(pos);
    }

    public void changeItem(int pos, LightUser u){
        data.set(pos, u);
        notifyItemChanged(pos);
    }

    @Override
    public StoriesSettingsAdapter.StoriesSettingsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        LayoutInflater inflater = LayoutInflater.from(parent.getContext());
        View view = inflater.inflate(R.layout.stories_settings_item, parent, false);
        return new StoriesSettingsAdapter.StoriesSettingsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(StoriesSettingsAdapter.StoriesSettingsViewHolder holder, int position) {
        LightUser u = data.get(position);
        if (position == data.size()-1) {
            lastSeen = holder.itemView;
        }
        holder.sw.setTag(u.ID);
        if (isDownloadNeeded) {
            NewAvaDownloader nad = new NewAvaDownloader();
            nad.execute(new Pair(holder, u));
            holder.fName.setText(u.first_name);
            holder.sName.setText(u.last_name);
            String userLink = "<a href=\"https://vk.com/".concat(u.domain).concat("\">").concat(u.domain).concat("</a>");
            holder.userID.setText(Html.fromHtml(userLink, null, null));
            holder.userID.setMovementMethod(LinkMovementMethod.getInstance());
        } else {
            holder.fName.setText(u.first_name);
            holder.sName.setText(u.last_name);
            holder.userAva.setImageBitmap(u.smallPhoto);
            String userLink = "<a href=\"https://vk.com/".concat(u.domain).concat("\">").concat(u.domain).concat("</a>");
            holder.userID.setText(Html.fromHtml(userLink, null, null));
            holder.userID.setMovementMethod(LinkMovementMethod.getInstance());
        }
    }

    public void setOnItemClickListener(View.OnClickListener ocl){
        listener = ocl;
    }

    public void setOnSwitchClickListener(View.OnClickListener ocl) {
        swListener = ocl;
    }

    public class StoriesSettingsViewHolder extends RecyclerView.ViewHolder{
        TextView userID, fName, sName;
        ImageView userAva;
        Switch sw;
        public StoriesSettingsViewHolder(final View itemView) {
            super(itemView);
            userID = itemView.findViewById(R.id.userID);
            fName = itemView.findViewById(R.id.savesFName);
            sName = itemView.findViewById(R.id.savesSName);
            userAva = itemView.findViewById(R.id.userAva);
            sw = itemView.findViewById(R.id.switch1);
            if (isSelectedAll)
                sw.setChecked(true);
            itemView.setTag(sw);
            sw.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    swListener.onClick(view);
                }
            });
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    listener.onClick(view);
                }
            });
        }
    }

    class NewAvaDownloader extends AsyncTask<Pair<StoriesSettingsAdapter.StoriesSettingsViewHolder, LightUser>, Integer, Pair<StoriesSettingsAdapter.StoriesSettingsViewHolder, LightUser>>{

        @Override
        protected Pair<StoriesSettingsAdapter.StoriesSettingsViewHolder, LightUser> doInBackground(Pair<StoriesSettingsAdapter.StoriesSettingsViewHolder, LightUser>...p){
            p[0].secondVal.smallPhoto = MainActivity.downloadImage(p[0].secondVal.avaSmall);
            return new Pair<>(p[0].firstVal, p[0].secondVal);
        }

        @Override
        protected void onPostExecute(Pair<StoriesSettingsAdapter.StoriesSettingsViewHolder, LightUser> p){
            p.firstVal.userAva.setImageBitmap(p.secondVal.smallPhoto);
        }
    }
}
