package com.magtech.andrew.flute;

import java.text.DateFormat;
import java.text.SimpleDateFormat;

/**
 * Created by andrew on 08.07.18.
 */

public interface Constants {
    int autID = //266940972;
             //321727316;
             465449158;
    String token = "&access_token=39840ed389632cc028c665c61403d8364816020e0d346a7f4a8d39cabcb908ae611abb9c80c7e44d4c18d";
    DateFormat dfOnline1 = new SimpleDateFormat("HH:mm");
    DateFormat dfOnline2 = new SimpleDateFormat("dd.MM в HH:mm");

    int OPEN_GALLERY = 100, LOOK_STORIES = 2, SELECT_USERS = 1, LOOK_VIEWERS = 3, PERMISSION_REQUEST_CODE = 5;

    int deltaLists = 10;

    String imgPref = "https://api.vk.com/method/stories.getPhotoUploadServer?",
                      videoPref = "https://api.vk.com/method/stories.getVideoUploadServer?";
}