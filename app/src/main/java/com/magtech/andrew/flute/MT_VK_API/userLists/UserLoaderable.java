package com.magtech.andrew.flute.MT_VK_API.userLists;

import com.magtech.andrew.flute.MT_VK_API.user.LightUser;
import java.util.ArrayList;

/**
 * Created by andrew on 08.07.18.
 */

public interface UserLoaderable {

    ArrayList<LightUser> getUsersList(String ID);
    String getMessage();
    String getName();
}
