package com.magtech.andrew.flute.MT_VK_API.userLists;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 23.07.18.
 */

public class StoriesNews implements UserLoaderable {

    String message = "message";
    ArrayList<LightUser> compare = null;

    public StoriesNews(ArrayList<LightUser> compare){
        this.compare = compare;
    }

    public StoriesNews(){
        this.compare = null;
    }

    @Override
    public String getName(){
        return "Новости";
    }

    public String getMessage(){
        return message;
    }

    public ArrayList<LightUser> getComparedUsersList(){
        ArrayList<LightUser> ans = new ArrayList<>();
        try{
            String url = "https://api.vk.com/method/stories.get?"+Constants.token.substring(0)+"&v=5.80";
            JsonObject object;
            URL address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            object = object.getJsonObject("response");
            int count = object.getInt("count");
            JsonArray items = object.getJsonArray("items");
            for (int i=0; i < count; i++){
                int id = items.getJsonArray(i).getJsonObject(0).getInt("owner_id");
                for (LightUser u:compare) {
                    if (u.ID == id) {
                        u.numStories = items.getJsonArray(i).size();
                        ans.add(u);
                    }
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }

    public ArrayList<LightUser> getUsersList(String id){
        if (compare != null)
            return getComparedUsersList();
        ArrayList<LightUser> ans = new ArrayList<>();
        try{
            String url = "https://api.vk.com/method/stories.get?"+Constants.token.substring(0)+"&extended=1&v=5.80";
            JsonObject object;
            URL address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            object = object.getJsonObject("response");
            int count = object.getInt("count");
            JsonArray profiles = object.getJsonArray("profiles");
            for (int i=0; i < count; i++){
                LightUser u = new LightUser(profiles.getJsonObject(i));
                ans.add(u);
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }
}
