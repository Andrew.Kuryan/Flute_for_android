package com.magtech.andrew.flute.MT_VK_API.userLists;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 08.07.18.
 */

public class Friends implements UserLoaderable {

    @Override
    public String getName(){
        return "Друзья";
    }

    @Override
    public String getMessage(){
        return "message";
    }

    public ArrayList<LightUser> getUsersList(String id){
        ArrayList<LightUser> ans = new ArrayList<>();
        int count;
        final String url1 = "https://api.vk.com/method/friends.get?user_id=".concat(id).
                concat(Constants.token).concat("&fields=photo_100,online,last_seen,domain&v=5.8");

        final String url2 = "https://api.vk.com/method/friends.get?user_id=".concat(id).concat("&offset=5000").
                concat(Constants.token).concat("&fields=photo_100,online,last_seen,domain&v=5.8");
        try{
            JsonObject object;
            URL address = new URL(url1);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            object = object.getJsonObject("response");
            JsonArray a = object.getJsonArray("items");
            count = object.getInt("count");
            for(int i = 0; i < (count <= 5000 ? count : 5000); i++){
                ans.add(new LightUser(a.getJsonObject(i)));
            }
            if(count > 5000){
                address = new URL(url2);
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                object = object.getJsonObject("response");
                a = object.getJsonArray("items");
                for(int i = 5000; i < count; i++){
                    ans.add(new LightUser(a.getJsonObject(i)));
                }
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }
}
