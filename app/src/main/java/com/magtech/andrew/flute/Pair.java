package com.magtech.andrew.flute;

/**
 * Created by andrew on 08.07.18.
 */

public class Pair<F, S> {

    public F firstVal;
    public S secondVal;

    public Pair(F first, S second){
        this.firstVal = first;
        this.secondVal = second;
    }
}
