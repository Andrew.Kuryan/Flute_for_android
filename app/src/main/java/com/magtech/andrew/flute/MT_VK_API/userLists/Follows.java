package com.magtech.andrew.flute.MT_VK_API.userLists;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 08.07.18.
 */

public class Follows implements UserLoaderable {

    @Override
    public String getName(){
        return "Подписчики";
    }

    @Override
    public String getMessage(){
        return "message";
    }

    public ArrayList<LightUser> getUsersList(String id){
        ArrayList<LightUser> ans = new ArrayList<>();
        int count, i = 0, j = 0;
        try{
            JsonObject object;
            String url1 = "https://api.vk.com/method/users.getFollowers?user_id=".concat(id).
                    concat(Constants.token).concat("&count=0&v=5.8");
            URL address = new URL(url1);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            object = object.getJsonObject("response");
            count = object.getInt("count");
            while(j < count) {
                String url2 = "https://api.vk.com/method/users.getFollowers?user_id=".concat(id).
                        concat(Constants.token).concat("&offset=").concat(Integer.toString(j)).
                        concat("&count=1000&fields=photo_100,online,last_seen,domain&v=5.8");
                address = new URL(url2);
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                object = object.getJsonObject("response");
                JsonArray a = object.getJsonArray("items");
                for (i = 0; i < a.size(); i++) {
                    ans.add(new LightUser(a.getJsonObject(i)));
                }
                j+=i;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }
}
