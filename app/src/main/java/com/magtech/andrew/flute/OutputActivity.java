package com.magtech.andrew.flute;

import android.annotation.SuppressLint;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.os.Handler;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.MediaController;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.magtech.andrew.flute.MT_VK_API.stories.Story;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.magtech.andrew.flute.MainActivity.downloadImage;

public class OutputActivity extends AppCompatActivity {

    private static final boolean AUTO_HIDE = true;

    private static final int AUTO_HIDE_DELAY_MILLIS = 2000;

    private static final int UI_ANIMATION_DELAY = 300;
    private final Handler mHideHandler = new Handler();
    private View mContentView;
    private final Runnable mHidePart2Runnable = new Runnable() {
        @SuppressLint("InlinedApi")
        @Override
        public void run() {
            mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LOW_PROFILE
                    | View.SYSTEM_UI_FLAG_FULLSCREEN
                    | View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY
                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION);
        }
    };
    private View mControlsView;
    private final Runnable mShowPart2Runnable = new Runnable() {
        @Override
        public void run() {
            mControlsView.setVisibility(View.VISIBLE);
        }
    };
    private boolean mVisible;
    private final Runnable mHideRunnable = new Runnable() {
        @Override
        public void run() {
            hide();
        }
    };

    private final View.OnTouchListener mDelayHideTouchListener = new View.OnTouchListener() {
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            if (AUTO_HIDE) {
                delayedHide(AUTO_HIDE_DELAY_MILLIS);
            }
            return false;
        }
    };

    Story arr[];
    int curPos = 0;
    Intent result = new Intent();

    View.OnClickListener onViewsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!tfViews.getText().equals("0"))
                startActivity(Integer.toString(arr[curPos].id));
        }
    };

    public void startActivity(String id){
        Intent intent = new Intent(this, ViewersActivity.class);
        intent.putExtra(this.getString(R.string.app_pref)+".story_id", id);
        startActivityForResult(intent, Constants.LOOK_VIEWERS);
    }

    final static DateFormat dfStory = new SimpleDateFormat("HH:mm");

    ProgressBar pbVideo, pbPosition;
    LinearLayout ll;
    ImageView imageView, ivViews;
    ImageLoader il;
    VideoView videoView;
    VideoLoader vl;
    Button butDelete, butNext, butPrev;
    TextView tfDate, tfViews;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_output);

        getSupportActionBar().hide();

        mVisible = true;
        mControlsView = findViewById(R.id.fullscreen_content_controls);
        mContentView = findViewById(R.id.fullscreen_content);

        mContentView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                toggle();
            }
        });

        mControlsView.setOnTouchListener(mDelayHideTouchListener);

        pbVideo = findViewById(R.id.videoProgress);
        pbPosition = findViewById(R.id.positionProgress);
        ll = findViewById(R.id.linear);
        butDelete = findViewById(R.id.butStoriesDelete);
        butDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getApplicationContext());
                builder.setMessage("Вы уверены, что хотите удалить историю?")
                        .setCancelable(false)
                        .setNegativeButton("Нет",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                })
                        .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                StoriesDeleter sd = new StoriesDeleter();
                                sd.execute(true);
                            }
                        });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        tfDate = findViewById(R.id.date);
        ivViews = findViewById(R.id.imageViews);
        ivViews.setOnClickListener(onViewsClickListener);
        tfViews = findViewById(R.id.textViews);
        tfViews.setOnClickListener(onViewsClickListener);
        butNext = findViewById(R.id.butNext);
        butPrev = findViewById(R.id.butPrev);

        ArrayList<Story> al = null;
        try {
            Bundle args = getIntent().getExtras();
            al = (ArrayList<Story>) args.getSerializable(this.getString(R.string.app_pref) + ".Stories");
        }catch (Exception exc){
            finish();
            exc.printStackTrace();
        }
        arr = new Story[al.size()];
        for (int i=0; i<al.size(); i++)
            arr[i] = al.get(i);
        curPos = 0;
        pbPosition.setMax(arr.length);
        pbPosition.setProgress(1);
        drawHistory(curPos);
    }

    public void closeActivity(String message){
        ArrayList<Story> arrayList = new ArrayList<>();
        Collections.addAll(arrayList, arr);
        result.putExtra(this.getString(R.string.app_pref)+".Stories", arrayList);
        result.putExtra(this.getString(R.string.app_pref)+".StoriesExtra", message);
        setResult(RESULT_OK, result);
        finish();
    }

    public void drawHistory(int pos){
        try {
            TextView date = findViewById(R.id.date);
            pbPosition.setProgress(curPos + 1);
            if (arr[pos].owner_id != Constants.autID) {
                butDelete.setVisibility(View.INVISIBLE);
                ivViews.setVisibility(View.INVISIBLE);
                tfViews.setVisibility(View.INVISIBLE);
            } else {
                butDelete.setVisibility(View.VISIBLE);
                ivViews.setVisibility(View.VISIBLE);
                tfViews.setVisibility(View.VISIBLE);
                tfViews.setText(Integer.toString(arr[pos].views));
            }
            switch (arr[pos].type) {
                case "photo":
                    ll.removeView(imageView);
                    if (vl != null && !vl.isCancelled())
                        vl.cancel(true);
                    if (videoView != null && videoView.isPlaying()) {
                        videoView.stopPlayback();
                        ll.removeView(videoView);
                    }
                    imageView = new ImageView(this);
                    imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);
                    il = new ImageLoader();
                    il.execute(arr[pos].url);
                    break;
                case "video":
                    ll.removeView(videoView);
                    if (il != null && !il.isCancelled())
                        il.cancel(true);
                    ll.removeView(imageView);

                    videoView = new VideoView(this);
                    MediaController mc = new MediaController(this);
                    mc.hide();
                    videoView.setMediaController(mc);
                    vl = new VideoLoader();
                    vl.execute(arr[pos].url);
                    break;
                default:
                    return;
            }
            Date d = new Date(arr[pos].date * 1000);
            date.setText(dfStory.format(d));
        }catch (Exception exc){
            exc.printStackTrace();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data){
        if (requestCode==Constants.LOOK_VIEWERS && resultCode==RESULT_OK){
            String id = "";
            try {
                Bundle args = data.getExtras();
                id = args.getString(this.getString(R.string.app_pref)+".selected_id");
            }catch (Exception exc){
                finish();
                exc.printStackTrace();
            }
            if (!id.equals("empty")){
                closeActivity(id);
            }
        }
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        closeActivity("empty");
    }

    public void onButNext(View v){
        if (curPos < arr.length-1){
            curPos++;
            drawHistory(curPos);
        }
        else{
            closeActivity("empty");
        }
    }

    public void onButPrev(View v){
        if (curPos > 0){
            curPos--;
            drawHistory(curPos);
        }
    }

    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);

        delayedHide(100);
    }

    private void toggle() {
        if (mVisible) {
            hide();
        } else {
            show();
        }
    }

    private void hide() {
        mControlsView.setVisibility(View.GONE);
        mVisible = false;

        mHideHandler.removeCallbacks(mShowPart2Runnable);
        mHideHandler.postDelayed(mHidePart2Runnable, UI_ANIMATION_DELAY);
    }

    @SuppressLint("InlinedApi")
    private void show() {
        mContentView.setSystemUiVisibility(View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION);
        mVisible = true;

        mHideHandler.removeCallbacks(mHidePart2Runnable);
        mHideHandler.postDelayed(mShowPart2Runnable, UI_ANIMATION_DELAY);
    }

    private void delayedHide(int delayMillis) {
        mHideHandler.removeCallbacks(mHideRunnable);
        mHideHandler.postDelayed(mHideRunnable, delayMillis);
    }

    class StoriesDeleter extends AsyncTask<Boolean, Integer, Boolean>{

        @Override
        protected void onPreExecute(){
            pbVideo.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(Boolean...param){
            int a = arr[curPos].delete();
            if (a==1)
                return true;
            else
                return false;
        }

        @Override
        protected void onPostExecute(Boolean b){
            if (b){
                Story arrNew[] = new Story[arr.length-1];
                for (int i=0; i<curPos; i++)
                    arrNew[i] = arr[i];
                for (int i=curPos+1; i<arr.length; i++)
                    arrNew[i-1] = arr[i];
                arr = arrNew;
                System.gc();
                pbPosition.setMax(arr.length);
                if (curPos < arr.length)
                    drawHistory(curPos);
                else {
                    closeActivity("empty");
                }
                Toast toast = Toast.makeText(getApplicationContext(), "Успешно удалено", Toast.LENGTH_SHORT);
                toast.show();
            }
            else{
                Toast toast = Toast.makeText(getApplicationContext(), "Ошибка удаления", Toast.LENGTH_SHORT);
                toast.show();
            }
        }
    }

    class VideoLoader extends AsyncTask<String, Integer, Boolean> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            ll.addView(videoView);
            pbVideo.setVisibility(View.VISIBLE);
        }

        @Override
        protected Boolean doInBackground(String...param){
            videoView.setVideoURI(Uri.parse(param[0]));
            videoView.start();
            while (!videoView.isPlaying()){}
            return true;
        }

        @Override
        protected void onPostExecute(Boolean b){
            super.onPostExecute(b);
            pbVideo.setVisibility(View.INVISIBLE);
        }
    }

    class ImageLoader extends AsyncTask<String, Integer, Bitmap>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            ll.addView(imageView);
            pbVideo.setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String...param){
            Bitmap bm = downloadImage(param[0]);
            return bm;
        }

        @Override
        protected void onPostExecute(Bitmap b){
            super.onPostExecute(b);
            if (imageView != null && b != null) {
                imageView.setImageBitmap(b);
            }
            pbVideo.setVisibility(View.INVISIBLE);
        }
    }
}
