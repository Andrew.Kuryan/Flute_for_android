package com.magtech.andrew.flute;

import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.util.LinkedList;
import java.util.Queue;

/**
 * Created by andrew on 09.07.18.
 */

class Port{

    Queue<LightUser> queue = new LinkedList<>();

    synchronized void add(LightUser u){
        queue.add(u);
    }

    synchronized LightUser get(){
        return queue.remove();
    }

    boolean isEmpty(){
        return queue.isEmpty();
    }
}
