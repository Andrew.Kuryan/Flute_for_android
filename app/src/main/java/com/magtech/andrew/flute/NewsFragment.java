package com.magtech.andrew.flute;

import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.NestedScrollView;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;

import com.magtech.andrew.flute.MT_VK_API.userLists.Friends;
import com.magtech.andrew.flute.MT_VK_API.userLists.StoriesNews;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.util.ArrayList;

public class NewsFragment extends Fragment {

    StoriesNews storiesNews;
    SwipeRefreshLayout srl;
    ProgressBar pbNews;
    RecyclerView savesList;
    NewsAdapter na;
    NestedScrollView nsv;
    ArrayList<LightUser> storiesList = new ArrayList<>();
    Port port = new Port();
    AdapterCreator ac;
    static int size;

    private MainInterface listener;

    View.OnClickListener onItemClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            String ID = Integer.toString((Integer) view.getTag());
            listener.setFragment(ProfileFragment.newInstance(ID), R.id.container);
        }
    };

    public static NewsFragment newInstance(){
        return new NewsFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_news, container, false);

        listener = (MainInterface) getActivity();

        listener.setToolbarTitle("Новости");
        srl = rootView.findViewById(R.id.swipe_saves);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                adapterPos = 0;
                avaPos = 0;
                storiesList.clear();
                na = null;
                System.gc();
                NewsDownloader nd = new NewsDownloader();
                nd.execute(true);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_orange_light, android.R.color.holo_red_light);

        NewsDownloader nd = new NewsDownloader();
        nd.execute(true);

        pbNews = rootView.findViewById(R.id.newsProgress);
        FloatingActionButton fabToStart = rootView.findViewById(R.id.fabToStart);
        fabToStart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nsv.scrollTo(0, 0);
            }
        });
        savesList = rootView.findViewById(R.id.savesList);
        savesList.setLayoutManager(new GridLayoutManager(getContext(), 2));
        nsv = rootView.findViewById(R.id.mNestedView);
        savesList.setItemAnimator(null);

        return rootView;
    }

    @Override
    public void onStop(){
        super.onStop();
        if (ac != null) {
            ac.cancel(true);
            ac = null;
        }
        System.gc();
    }

    class NewsDownloader extends AsyncTask<Boolean, Integer, Boolean>{

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            srl.setRefreshing(true);
        }

        @Override
        protected Boolean doInBackground(Boolean...param){
            ArrayList<LightUser> usersList = new Friends().getUsersList(Integer.toString(Constants.autID));
            storiesNews = new StoriesNews(usersList);
            storiesList = storiesNews.getUsersList("compare");
            size = storiesList.size();
            return true;
        }

        @Override
        protected void onPostExecute(Boolean p){
            super.onPostExecute(p);
            na = new NewsAdapter(null);
            na.setOnItemClickListener(onItemClickListener);
            savesList.setAdapter(na);
            if (size != 0) {
                pbNews.setVisibility(View.VISIBLE);
                pbNews.setMax(size);
                pbNews.setProgress(0);
                ac = new AdapterCreator();
                ac.execute(true);
                alt = new AvaLoaderThread();
                new Thread(alt).start();
            }
            else{
                srl.setRefreshing(false);
            }
        }
    }

    int adapterPos = 0;

    class AdapterCreator extends AsyncTask<Boolean, Integer, LightUser>{

        @Override
        protected LightUser doInBackground(Boolean...param){
            LightUser u = storiesList.get(adapterPos);
            return u;
        }

        @Override
        protected void onPostExecute(LightUser u){
            super.onPostExecute(u);
            pbNews.setProgress(adapterPos+1);
            na.insertItem(adapterPos, u);
            port.add(u);
            if (adapterPos < size-1) {
                adapterPos++;
                ac = new AdapterCreator();
                ac.execute(true);
                System.gc();
            }
            else {
                LightUser l = new LightUser();
                l.ID = 0;
                port.add(l);
                pbNews.setVisibility(View.INVISIBLE);
            }
        }
    }

    Handler avaLoaderHandler = new Handler();
    AvaLoaderThread alt;
    int avaPos = 0;

    class AvaLoaderThread implements Runnable{

        @Override
        public void run() {
            while (port.isEmpty()) {}
            final LightUser u = port.get();
            final int a = avaPos;
            if (u.ID != 0) {
                u.smallPhoto = MainActivity.downloadImage(u.avaSmall);
                avaLoaderHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        na.changeItem(a, u);
                    }
                });
                avaPos++;
                alt = new AvaLoaderThread();
                new Thread(alt).start();
            }
            else{
                avaLoaderHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        srl.setRefreshing(false);
                    }
                });
            }
        }
    }
}
