package com.magtech.andrew.flute.MT_VK_API.userLists;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 23.07.18.
 */

public class StoryViewers implements UserLoaderable {

    String message = "message";
    ArrayList<LightUser> compare = null;

    public StoryViewers(ArrayList<LightUser> compare){
        this.compare = compare;
    }

    public StoryViewers(){
        this.compare = null;
    }

    @Override
    public String getName(){
        return "Просмотры";
    }

    @Override
    public String getMessage(){
        return message;
    }

    //"https://api.vk.com/method/stories.getViewers?owner_id=465449158&story_id=1015797&access_token=39840ed389632cc028c665c61403d8364816020e0d346a7f4a8d39cabcb908ae611abb9c80c7e44d4c18d&offset=0&count=1000&v=5.80".


    public ArrayList<LightUser> getComparedUsersList(String id){
        ArrayList<LightUser> ans = new ArrayList<>();
        int count, i = 0, j = 0;
        try{
            JsonObject object;
            String url1 = "https://api.vk.com/method/stories.getViewers?owner_id=".
                    concat(Integer.toString(Constants.autID)).
                    concat("&story_id=").concat(id).
                    concat(Constants.token).concat("&offset=0&count=0&v=5.80");
            URL address = new URL(url1);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            object = object.getJsonObject("response");
            count = object.getInt("count");
            while(j < count) {
                String url2 = "https://api.vk.com/method/stories.getViewers?owner_id=".
                        concat(Integer.toString(Constants.autID)).
                        concat("&story_id=").concat(id).
                        concat(Constants.token).concat("&offset=").concat(Integer.toString(j)).
                        concat("&count=1000&v=5.80");
                address = new URL(url2);
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                object = object.getJsonObject("response");
                JsonArray a = object.getJsonArray("items");
                for (i = 0; i < a.size(); i++) {
                    for (LightUser u:compare){
                        if (u.ID == a.getInt(i)){
                            ans.add(u);
                        }
                    }
                }
                j+=i;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }

    //https://vk.com/id465449158?w=story465449158_1015797%2Fprofile

    public ArrayList<LightUser> getUsersList(String id){
        if (compare != null)
            return getComparedUsersList(id);
        ArrayList<LightUser> ans = new ArrayList<>();
        int count, i = 0, j = 0;
        try{
            JsonObject object;
            String url1 = "https://api.vk.com/method/stories.getViewers?owner_id=".
                    concat(Integer.toString(Constants.autID)).
                    concat("&story_id=").concat(id).
                    concat(Constants.token).concat("&count=0&v=5.80");
            URL address = new URL(url1);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            object = object.getJsonObject("response");
            count = object.getInt("count");
            while(j < count) {
                String url2 = "https://api.vk.com/method/stories.getViewers?owner_id=".
                        concat(Integer.toString(Constants.autID)).
                        concat("&story_id=").concat(id).
                        concat(Constants.token).concat("&offset=").concat(Integer.toString(j)).
                        concat("&count=1000&extended=1&v=5.80");
                address = new URL(url2);
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                object = object.getJsonObject("response");
                JsonArray a = object.getJsonArray("items");
                for (i = 0; i < a.size(); i++) {
                    ans.add(new LightUser(a.getJsonObject(i)));
                }
                j+=i;
            }
        }catch(Exception e){
            e.printStackTrace();
        }
        return ans;
    }
}
