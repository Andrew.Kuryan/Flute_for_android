package com.magtech.andrew.flute;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.TextView;

import com.magtech.andrew.flute.MT_VK_API.userLists.Follows;
import com.magtech.andrew.flute.MT_VK_API.userLists.Friends;
import com.magtech.andrew.flute.MT_VK_API.userLists.StoryViewers;
import com.magtech.andrew.flute.MT_VK_API.user.LightUser;

import java.util.ArrayList;

public class ViewersActivity extends AppCompatActivity implements MainInterface{

    ActionBar ab;
    String id;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_viewers);
        ab = getSupportActionBar();
        setToolbarTitle("Просмотры");
        try {
            Bundle args = getIntent().getExtras();
            id = args.getString(this.getString(R.string.app_pref)+".story_id");
        }catch (Exception exc){
            finish();
            exc.printStackTrace();
        }
        ListsDownloader ld = new ListsDownloader();
        ld.execute(true);
    }

    @Override
    public void setToolbarTitle(String title){
        ab.setTitle(title);
    }

    @Override
    public void setFragment(Fragment fragment, int container){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();
        fragmentTransaction.replace(container, fragment);
        fragmentTransaction.commit();
    }

    @Override
    public void onBackPressed(){
        //super.onBackPressed();
        closeActivity("empty");
    }

    public void closeActivity(String id){
        Intent intent = new Intent();
        intent.putExtra(this.getString(R.string.app_pref)+".selected_id", id);
        setResult(RESULT_OK, intent);
        finish();
    }

    class ListsDownloader extends AsyncTask<Boolean, Integer, ArrayList<LightUser>>{

        @Override
        protected ArrayList<LightUser> doInBackground(Boolean...param){
            ArrayList<LightUser> friendsList = new Friends().getUsersList(Integer.toString(Constants.autID));
            ArrayList<LightUser> followsList = new Follows().getUsersList(Integer.toString(Constants.autID));
            friendsList.addAll(followsList);
            return friendsList;
        }

        @Override
        protected void onPostExecute(ArrayList<LightUser> al){
            super.onPostExecute(al);
            final FriendsFragment fragment = FriendsFragment.newInstance(id, new StoryViewers(al));
            fragment.setOnItemClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    final View item = view;
                    AlertDialog.Builder builder = new AlertDialog.Builder(fragment.getContext());
                    builder.setMessage("Вы уверены, что хотите завершить просмотр историй?")
                            .setCancelable(false)
                            .setNegativeButton("Нет",
                                    new DialogInterface.OnClickListener() {
                                        public void onClick(DialogInterface dialog, int id) {
                                            dialog.cancel();
                                        }
                                    })
                            .setPositiveButton("Да", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    String ID = ((TextView) item.getTag()).getText().toString();
                                    closeActivity(ID);
                                }
                            });
                    AlertDialog alert = builder.create();
                    alert.show();
                }
            });
            setFragment(fragment, R.id.vContainer);
        }
    }
}
