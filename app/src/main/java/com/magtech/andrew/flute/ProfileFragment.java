package com.magtech.andrew.flute;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.text.Html;
import android.text.method.LinkMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.magtech.andrew.flute.MT_VK_API.userLists.Follows;
import com.magtech.andrew.flute.MT_VK_API.userLists.Friends;
import com.magtech.andrew.flute.MT_VK_API.user.User;
import com.magtech.andrew.flute.MT_VK_API.stories.Stories;
import com.magtech.andrew.flute.MT_VK_API.stories.Story;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;

import static com.magtech.andrew.flute.MainActivity.downloadImage;

/**
 * Created by andrew on 10.06.18.
 */

public class ProfileFragment extends Fragment{

    public static String ID;

    View rootView;
    SwipeRefreshLayout srl;
    ProgressBar pbAva;
    TextView textStories, textFriends, textFollows;
    ImageView userPhoto;

    public User curUser;
    boolean isChanged = false;
    private MainInterface listener;

    public static ProfileFragment newInstance(String userID) {
        ProfileFragment fragment = new ProfileFragment();
        ID = userID;
        return fragment;
    }

    View.OnClickListener onFriendsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            listener.setFragment(FriendsFragment.newInstance(Integer.toString(curUser.ID),
                    new Friends()), R.id.container);
        }
    };

    View.OnClickListener onFollowsClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            listener.setFragment(FriendsFragment.newInstance(Integer.toString(curUser.ID),
                    new Follows()), R.id.container);
        }
    };

    View.OnClickListener onStoriesClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            try {
                Story arrStories[] = curUser.arrStories.storiesArray;
                if (arrStories != null) {
                    Intent intent = new Intent(getActivity(), OutputActivity.class);
                    ArrayList<Story> al = new ArrayList<Story>();
                    Collections.addAll(al, arrStories);
                    intent.putExtra(getActivity().getString(R.string.app_pref) + ".Stories", al);
                    startActivityForResult(intent, Constants.LOOK_STORIES);
                }
            }catch (NullPointerException exc){
                exc.printStackTrace();
            }
        }
    };

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_profile, container, false);

        listener = (MainInterface) getActivity();

        listener.setToolbarTitle("Профиль");

        srl = rootView.findViewById(R.id.swipe_profile);
        srl.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                createUser(ID);
            }
        });
        srl.setColorSchemeResources(android.R.color.holo_orange_light, android.R.color.holo_red_light);

        Button butFriends = rootView.findViewById(R.id.butFriends);
        butFriends.setOnClickListener(onFriendsClickListener);
        textFriends = rootView.findViewById(R.id.textFriends);
        Button butStories = rootView.findViewById(R.id.butStories);
        butStories.setOnClickListener(onStoriesClickListener);
        textStories = rootView.findViewById(R.id.textStories);
        Button butFollows = rootView.findViewById(R.id.butFollows);
        butFollows.setOnClickListener(onFollowsClickListener);
        textFollows = rootView.findViewById(R.id.textFollows);

        pbAva = rootView.findViewById(R.id.avaProgress);
        userPhoto = rootView.findViewById(R.id.userPhoto);

        createUser(ID);

        return rootView;
    }

    public void createUser(String ID){
        isChanged = false;
        UserCreator uc = new UserCreator();
        uc.execute(ID);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data){
        if (requestCode == Constants.LOOK_STORIES){
            String message = data.getExtras().getString(this.getString(R.string.app_pref)+".StoriesExtra");
            if (!message.equals("empty")){
                listener.setFragment(ProfileFragment.newInstance(message), R.id.container);
            }
            ArrayList<Story> arrayList = (ArrayList<Story>) data.getExtras().getSerializable(this.getString(R.string.app_pref)+".Stories");
            Story arr[] = new Story[arrayList.size()];
            for (int i=0; i<arrayList.size(); i++)
                arr[i] = arrayList.get(i);
            curUser.arrStories.storiesArray = arr;
            curUser.numStories = arr.length;
            if (arr.length != 0)
                textStories.setText(Integer.toString(arr.length));
            else{
                textStories.setText("-");
                curUser.arrStories = null;
                System.gc();
            }
        }
    }

    public void setCurUser(){
        try {
            TextView firstName = rootView.findViewById(R.id.firstName);
            TextView secondName = rootView.findViewById(R.id.secondName);
            final TextView online = rootView.findViewById(R.id.online);
            final TextView status = rootView.findViewById(R.id.textStatus);
            status.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
                @Override public void onGlobalLayout() {
                    if (!isChanged) {
                        if (status.getLineCount() > 2)
                            status.setTextSize(12);
                        else
                            status.setTextSize(15);
                        isChanged = true;
                    }
                }
            });
            TextView screenName = rootView.findViewById(R.id.screenName);

            firstName.setText(curUser.first_name);
            secondName.setText(curUser.last_name);
            if (curUser.online == 1)
                online.setText("Онлайн");
            else {
                Date d = new Date(curUser.last_seen * 1000);
                Date cur = new Date(System.currentTimeMillis());
                if (d.getDay() == cur.getDay() && d.getMonth() == cur.getMonth() && d.getYear() == cur.getYear())
                    online.setText("В сети: " + Constants.dfOnline1.format(d));
                else
                    online.setText("В сети: " + Constants.dfOnline2.format(d));
            }
            String userLink = "<a href=\"https://vk.com/" + curUser.domain + "\">" + curUser.domain + "</a>";
            if (!curUser.status.isEmpty()){
                status.setText(curUser.status);
                screenName.setText(Html.fromHtml(userLink, null, null));
                screenName.setMovementMethod(LinkMovementMethod.getInstance());
                screenName.setVisibility(View.VISIBLE);
            }
            else{
                status.setText(Html.fromHtml(userLink, null, null));
                status.setMovementMethod(LinkMovementMethod.getInstance());
                screenName.setVisibility(View.INVISIBLE);
            }
            if (curUser.numFriends == 0) textFriends.setText("-");
            else textFriends.setText(Integer.toString(curUser.numFriends));
            if (curUser.numFollows == 0) textFollows.setText("-");
            else textFollows.setText(Integer.toString(curUser.numFollows));
        }catch (NullPointerException exc){
            exc.printStackTrace();
        }
    }

    class UserCreator extends AsyncTask<String, Integer, User> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            userPhoto.setImageBitmap(null);
            srl.setRefreshing(true);
        }

        @Override
        protected User doInBackground(String...param) {
            System.out.println("PROFILE");
            curUser = new User(param[0]);
            System.out.println(curUser);
            return curUser;
        }

        @Override
        protected void onPostExecute(User u) {
            super.onPostExecute(u);
            setCurUser();
            StoriesDownloader sd = new StoriesDownloader();
            sd.execute(curUser.ID);
            AvaDownloader ad = new AvaDownloader();
            ad.execute(curUser.avaSmall, curUser.avaBig);
            srl.setRefreshing(false);
        }
    }

    class AvaDownloader extends AsyncTask<String, Integer, Bitmap> {

        @Override
        protected void onPreExecute(){
            super.onPreExecute();
            pbAva.setVisibility(View.VISIBLE);
        }

        @Override
        protected Bitmap doInBackground(String...param) {
            curUser.smallPhoto = downloadImage(param[0]);
            if (param.length > 1)
                curUser.bigPhoto = downloadImage(param[1]);
            return curUser.bigPhoto;
        }

        @Override
        protected void onPostExecute(Bitmap bm) {
            super.onPostExecute(bm);
            if (curUser.bigPhoto != null) {
                userPhoto.setImageBitmap(curUser.bigPhoto);
                userPhoto.setVisibility(View.VISIBLE);
            }
            pbAva.setVisibility(View.INVISIBLE);
        }
    }

    class StoriesDownloader extends AsyncTask<Integer, Integer, Integer>{
        @Override
        protected void onPreExecute(){
            super.onPreExecute();
        }

        @Override
        protected Integer doInBackground(Integer...param) {
            curUser.arrStories = new Stories(param[0]);
            if (curUser.arrStories == null)
                curUser.numStories = 0;
            else
                curUser.numStories = curUser.arrStories.length;
            System.out.println("STORIES: "+curUser.numStories);
            return curUser.numStories;
        }

        @Override
        protected void onPostExecute(Integer n) {
            super.onPostExecute(n);
            if (n == 0)
                textStories.setText("-");
            else
                textStories.setText(Integer.toString(curUser.numStories));
        }
    }
}
