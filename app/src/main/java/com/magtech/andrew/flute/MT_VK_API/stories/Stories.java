package com.magtech.andrew.flute.MT_VK_API.stories;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.stories.Story;
import com.magtech.andrew.flute.MT_VK_API.requestErrors;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;
import java.io.InputStream;
import java.net.URL;

public class Stories{
    public int length = 0;
    public Story[] storiesArray;

    public Stories(int id) {
        JsonObject object;
        String url = "https://api.vk.com/method/stories.get?owner_id=" + id + Constants.token + "&v=5.80";
        URL address = null;
        try {
            address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            JsonArray arrayOfStories = object.getJsonObject("response").getJsonArray("items").getJsonArray(0);
            storiesArray = new Story[arrayOfStories.size()];
            for (int i = 0; i < storiesArray.length; i++) {
                try {
                    storiesArray[i] = new Story(arrayOfStories.getJsonObject(i));
                }catch (Exception exc){}
            }
            length = storiesArray.length;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    static int getStoriesCount(String ID){
        int ans = 0;
        JsonObject object;
        String url = "https://api.vk.com/method/stories.get?owner_id=" + ID +
                Constants.token + "&v=5.74";
        URL address = null;
        try {
            address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            ans = object.getJsonObject("response").getJsonArray("items").getJsonArray(0).size();
            System.out.println("NUM: "+ans);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ans;
    }
}
