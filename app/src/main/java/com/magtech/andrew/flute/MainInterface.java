package com.magtech.andrew.flute;

import android.support.v4.app.Fragment;

/**
 * Created by andrew on 15.07.18.
 */

public interface MainInterface {

    void setToolbarTitle(String title);
    void setFragment(Fragment fragment, int container);
}
