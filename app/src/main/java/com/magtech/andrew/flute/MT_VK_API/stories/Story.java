package com.magtech.andrew.flute.MT_VK_API.stories;

import com.magtech.andrew.flute.Constants;
import com.magtech.andrew.flute.MT_VK_API.requestErrors;

import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;

import javax.json.Json;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.json.JsonReader;

/**
 * Created by andrew on 24.07.18.
 */

public class Story implements Serializable {

    public int id, owner_id, views;
    public String type, url;
    public long date;

    public Story(JsonObject object){
        id = object.getInt("id");
        owner_id = object.getInt("owner_id");
        if (object.containsKey("views"))
            views = object.getInt("views");
        type = object.getString("type");
        date = object.getInt("date");
        object = object.getJsonObject(type);
        String urly = "", urlz = "", urlw = "";
        if(type.equals("photo")){
            JsonArray sizes = object.getJsonArray("sizes");
            for (int i=0; i<sizes.size(); i++){
                if (sizes.getJsonObject(i).getString("type").equals("y"))
                    urly = sizes.getJsonObject(i).getString("url");
                else if (sizes.getJsonObject(i).getString("type").equals("z"))
                    urlz = sizes.getJsonObject(i).getString("url");
                else if (sizes.getJsonObject(i).getString("type").equals("w"))
                    urlw = sizes.getJsonObject(i).getString("url");
            }
            if (!urlw.isEmpty())
                url = urlw;
            else if (!urlz.isEmpty())
                url = urlz;
            else if (!urly.isEmpty())
                url = urly;
        }else{
            object = object.getJsonObject("files");
            if (object.containsKey("mp4_720"))
                url = object.getString("mp4_720");
            else if (object.containsKey("mp4_480"))
                url = object.getString("mp4_480");
            else if (object.containsKey("mp4_360"))
                url = object.getString("mp4_360");
            else if (object.containsKey("mp4_240"))
                url = object.getString("mp4_240");
        }
    }

    public int delete(){
        int ans = 0;
        JsonObject object;
        String url = "https://api.vk.com/method/stories.delete?owner_id=" + Integer.toString(owner_id) + "&story_id=" + Integer.toString(id) +
                Constants.token + "&v=5.80";
        System.out.println(url);
        URL address = null;
        try {
            address = new URL(url);
            InputStream is = address.openStream();
            JsonReader jsonReader = Json.createReader(is);
            object = jsonReader.readObject();
            jsonReader.close();
            is.close();
            while (requestErrors.check(object)) {
                is = address.openStream();
                jsonReader = Json.createReader(is);
                object = jsonReader.readObject();
                jsonReader.close();
                is.close();
            }
            ans = object.getInt("response");
        } catch (Exception e) {
            e.printStackTrace();
        }
        return ans;
    }
}
