package com.magtech.andrew.flute;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import java.io.BufferedInputStream;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;

public class MainActivity extends AppCompatActivity implements MainInterface{

    BottomNavigationView bnv;
    ActionBar ab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ab = getSupportActionBar();

        bnv = findViewById(R.id.navigation);
        bnv.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()){
                    case R.id.action_item1:
                        setFragment(ProfileFragment.newInstance(Integer.toString(Constants.autID)),
                                R.id.container);
                        break;
                    case R.id.action_item2:
                        setFragment(NewsFragment.newInstance(),
                                R.id.container);
                        break;
                    case R.id.action_item3:
                        setFragment(AdditionFragment.newInstance(),
                                R.id.container);
                        break;
                }
                return true;
            }
        });

        setFragment(ProfileFragment.newInstance(Integer.toString(Constants.autID)), R.id.container);
    }

    @Override
    public void setToolbarTitle(String title){
        if (title.equals("Профиль"))
            ab.hide();
        else {
            ab.show();
            ab.setTitle(title);
        }
    }

    @Override
    public void onBackPressed() {
        getSupportFragmentManager().popBackStack();
    }

    @Override
    public void setFragment(Fragment fragment, int container){
        FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction= fragmentManager.beginTransaction();

        fragmentTransaction.addToBackStack("Stack");
        fragmentTransaction.replace(container, fragment);
        fragmentTransaction.commit();
    }

    public static Bitmap downloadImage(String iUrl) {
        Bitmap bitmap = null;
        HttpURLConnection conn = null;
        BufferedInputStream buf_stream = null;
        try {
            conn = (HttpURLConnection) new URL(iUrl).openConnection();
            conn.setDoInput(true);
            conn.setRequestProperty("Connection", "Keep-Alive");
            conn.connect();
            buf_stream = new BufferedInputStream(conn.getInputStream());
            bitmap = BitmapFactory.decodeStream(buf_stream);
            buf_stream.close();
            conn.disconnect();
            buf_stream = null;
            conn = null;
        } catch (Exception exc) {
            return null;
        } finally {
            if (buf_stream != null)
                try {
                    buf_stream.close();
                } catch (IOException ex) {}
            if (conn != null)
                conn.disconnect();
        }
        return bitmap;
    }
}
