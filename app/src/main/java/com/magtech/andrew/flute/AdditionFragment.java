package com.magtech.andrew.flute;

import android.Manifest;
import android.app.Activity;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.media.MediaMetadataRetriever;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.VideoView;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.Objects;

import cz.msebera.android.httpclient.Header;

public class AdditionFragment extends Fragment {

    Button button1, button2;
    Switch swSettings;
    ImageView img;
    VideoView video;
    TextView textFile;
    ProgressBar progressBar;
    String requestString_GET, requestString_POST;
    JSONObject object;
    int[] user_ids = null;
    boolean add_to_news = true;
    String type, path;
    RequestSender requestSender = new RequestSender();
    Uri uri;

    private MainInterface listener;

    View.OnClickListener openInGallery = new View.OnClickListener() {
        @Override
        public void onClick(View v) {
            startActivity(new Intent(Intent.ACTION_VIEW, uri));
        }
    };

    View.OnClickListener onFileSelectClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (!hasPermissions())
                requestPermissionWithRationale();
            else {
                Intent gallery = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI);
                gallery.setType("image/jpg image/png image/gif video/mp4");
                startActivityForResult(gallery, Constants.OPEN_GALLERY);
            }
        }
    };

    View.OnClickListener onSwitchClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            if (swSettings.isChecked()){
                add_to_news = true;
                user_ids = null;
                System.gc();
            }
            else{
                add_to_news = false;
                Intent intent = new Intent(getActivity(), StoriesSettingsActivity.class);
                startActivityForResult(intent, Constants.SELECT_USERS);
            }
        }
    };

    public static AdditionFragment newInstance(){
        return new AdditionFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState){
        View rootView = inflater.inflate(R.layout.fragment_addition, container, false);

        listener = (MainInterface) getActivity();

        listener.setToolbarTitle("Добавление");
        button1 = rootView.findViewById(R.id.button);
        button2 = rootView.findViewById(R.id.button2);
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast toast = Toast.makeText(getContext(), "Выберите файл", Toast.LENGTH_SHORT);
                toast.show();
            }
        });
        Button button4 = rootView.findViewById(R.id.button4);
        button4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
                builder.setTitle("Поддерживаемые форматы")
                        .setMessage("Изображения:\n - png\n - jpg\n - gif\n\nВидео:\n - h.264")
                        .setCancelable(false)
                        .setNegativeButton("ОК",
                                new DialogInterface.OnClickListener() {
                                    public void onClick(DialogInterface dialog, int id) {
                                        dialog.cancel();
                                    }
                                });
                AlertDialog alert = builder.create();
                alert.show();
            }
        });
        swSettings = rootView.findViewById(R.id.switchSettings);
        img =     rootView.findViewById(R.id.imageView);
        video =   rootView.findViewById(R.id.videoView);
        textFile = rootView.findViewById(R.id.textFile);
        progressBar = rootView.findViewById(R.id.progressBar2);
        video.setOnClickListener(openInGallery);
        img.setOnClickListener(openInGallery);

        button1.setOnClickListener(onFileSelectClickListener);
        swSettings.setOnClickListener(onSwitchClickListener);

        return rootView;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, final Intent data){
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == Constants.OPEN_GALLERY && resultCode == Activity.RESULT_OK) {
            onChoosingFile(data);
        }
        if(requestCode == Constants.SELECT_USERS) {
            int arr[] = data.getExtras().getIntArray(this.getString(R.string.app_pref) + ".Users");
            setUser_ids(arr);
            Toast toast = Toast.makeText(getActivity(),
                    new String("Выбрано пользователей: "+Integer.toString(arr.length)), Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    private boolean hasPermissions(){
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        for (String perms : permissions){
            if (!(getActivity().checkCallingOrSelfPermission(perms) == PackageManager.PERMISSION_GRANTED)){
                return false;
            }
        }
        return true;
    }

    private void requestPerms(){
        String[] permissions = new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE};
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M){
            requestPermissions(permissions, Constants.PERMISSION_REQUEST_CODE);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        boolean allowed = true;

        switch (requestCode){
            case Constants.PERMISSION_REQUEST_CODE:
                for (int res : grantResults){
                    allowed = allowed && (res == PackageManager.PERMISSION_GRANTED);
                }
                break;
            default:
                allowed = false;
                break;
        }
        if (!allowed) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (shouldShowRequestPermissionRationale(Manifest.permission.WRITE_EXTERNAL_STORAGE)){
                    Toast.makeText(getContext(), "Доступ к памяти отклонен", Toast.LENGTH_SHORT).show();
                } else {
                    showNoStoragePermissionSnackbar();
                }
            }
        }
    }

    public void showNoStoragePermissionSnackbar() {
        Snackbar.make(getActivity().findViewById(R.id.navigation), "Доступ к памяти не предоставлен" , Snackbar.LENGTH_LONG)
                .setAction("НАСТРОЙКИ", new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openApplicationSettings();
                        Toast.makeText(getContext(),
                                "Откройте разрешения и предоставьте доступ к памяти",
                                Toast.LENGTH_SHORT)
                                .show();
                    }
                })
                .show();
    }

    public void openApplicationSettings() {
        Intent appSettingsIntent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                Uri.parse("package:" + getActivity().getPackageName()));
        startActivityForResult(appSettingsIntent, Constants.PERMISSION_REQUEST_CODE);
    }

    public void requestPermissionWithRationale() {
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
            final String message = "Для добавления новых историй необходим доступ к памяти";
            Snackbar.make(getActivity().findViewById(R.id.navigation), message, Snackbar.LENGTH_LONG)
                    .setAction("РАЗРЕШИТЬ", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            requestPerms();
                        }
                    })
                    .show();
        } else {
            requestPerms();
        }
    }

    public void setUser_ids(int[] arr){
        if (arr == null || arr.length==0){
            swSettings.setChecked(true);
            add_to_news = true;
            user_ids = null;
            System.gc();
        }
        else {
            user_ids = arr;
            System.out.print("GET USERS: ");
            for (int i = 0; i < user_ids.length; i++) {
                System.out.print(user_ids[i] + " ");
            }
        }
    }

    public void onChoosingFile(final Intent data){
        button2.setEnabled(false);
        uri = data.getData();
        textFile.setText(getRealPathFromURI(uri));
        video.destroyDrawingCache();
        img.destroyDrawingCache();
        if (Objects.requireNonNull(getActivity().getContentResolver().getType(Objects.requireNonNull(data.getData()))).contains("image")) {
            img.setImageURI(data.getData());
            img.setVisibility(View.VISIBLE);
            video.setVisibility(View.INVISIBLE);
            type = "file";
            button2.setEnabled(true);
        } else {
            video.setVideoURI(data.getData());
            video.setMediaController(null);
            video.requestFocus(0);
            video.setVisibility(View.VISIBLE);
            video.seekTo(100);
            img.setVisibility(View.INVISIBLE);
            type = "video_file";
            if(checkVideo(data.getData()))
                button2.setEnabled(true);
            else {
                Toast toast = Toast.makeText(getActivity(), R.string.uvf, Toast.LENGTH_LONG);
                toast.show();
            }
        }
        button2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                requestString_GET = type.equals("file") ? Constants.imgPref : Constants.videoPref;
                if (add_to_news) {
                    requestString_GET += "add_to_news=1";
                } else {
                    requestString_GET += "user_ids=";
                    for (int i=0; i<user_ids.length-1; i++){
                        requestString_GET = requestString_GET.concat(Integer.toString(user_ids[i])).concat(",");
                    }
                    requestString_GET = requestString_GET.concat(Integer.toString(user_ids[user_ids.length-1]));
                }
                requestString_GET += Constants.token + "&v=5.80";

                System.out.println(requestString_GET);

                path = getRealPathFromURI(data.getData());
                progressBar.setVisibility(View.VISIBLE);
                object = requestSender.RequestGET(requestString_GET);
            }
        });
    }

    public String getRealPathFromURI(Uri contentUri) {
        String[] proj = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().getContentResolver().query(contentUri, proj, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public void sendImage(){
        System.out.println("sendImage1: "+object.toString());
        try {
            requestString_POST = object.getJSONObject("response").getString("upload_url");
            System.out.println("sendImage2: "+requestSender.responsePOST(requestString_POST, new File(path), type));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    boolean checkVideo(Uri file){
        MediaMetadataRetriever mediaMetadataRetriever = new MediaMetadataRetriever();
        mediaMetadataRetriever.setDataSource(getActivity(), file);
        String w = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_WIDTH);  // < 720
        String h = mediaMetadataRetriever.extractMetadata(MediaMetadataRetriever.METADATA_KEY_VIDEO_HEIGHT); // < 1280
        long fileSize = new File(getRealPathFromURI(file)).length(); // < 10MB
        float fps; // < 30
        String codec; // == "h.264"
        return false;
    }

    class RequestSender {

        private AsyncHttpClient client;
        private JSONObject ans;

        RequestSender(){
            client = new AsyncHttpClient();
        }

        JSONObject RequestGET(String url){
            client.get(url, new AsyncHttpResponseHandler() {

                @Override
                public void onStart() {
                    System.out.println("Sending request");
                }

                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] response) {
                    try {
                        ans = new JSONObject(new String(response));
                        object = ans;
                        sendImage();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] errorResponse, Throwable e) {
                    System.out.println("Request is failed");
                    System.out.println(new String(errorResponse));
                }

                @Override
                public void onRetry(int retryNo) {}
            });
            client.cancelAllRequests(true);
            return ans;
        }

        JSONObject responsePOST(String url, File file, String type) throws FileNotFoundException {
            RequestParams params = new RequestParams();
            params.put(type, file);
            client.post(url, params, new AsyncHttpResponseHandler() {
                @Override
                public void onSuccess(int statusCode, Header[] headers, byte[] responseBody) {
                    try {
                        ans = new JSONObject(new String(responseBody));
                        System.out.println("ANSWER: "+ans.toString());
                        progressBar.setVisibility(View.INVISIBLE);
                        Toast toast = Toast.makeText(getContext(), "Успешно добавлено", Toast.LENGTH_LONG);
                        toast.show();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }

                @Override
                public void onFailure(int statusCode, Header[] headers, byte[] responseBody, Throwable error) {
                    System.out.println(new String(responseBody));
                    progressBar.setVisibility(View.INVISIBLE);
                    Toast toast = Toast.makeText(getContext(), "Ошибка добавления", Toast.LENGTH_LONG);
                    toast.show();
                    error.printStackTrace();
                }
            });
            client.cancelAllRequests(true);
            return ans;
        }
    }
}
